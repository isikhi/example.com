import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
    {
        title: 'Jobs',
        imageUrl: 'img/undraw_docusaurus_mountain.svg',
        description: (
            <>
                You can find job opportunities in technology companies in software, digital product development, or you
                can offer job opportunities that we can work with.
            </>
        ),
    },
    {
        title: 'Blog',
        imageUrl: 'img/undraw_docusaurus_tree.svg',
        description: (
            <>
                You can reach my blogs that I wrote thinking that they will contribute to the community on software
                development, digital product development, startup and career.
            </>
        ),
    },
    {
        title: 'Experience',
        imageUrl: 'img/undraw_docusaurus_react.svg',
        description: (
            <>
                Can examine the institutions and projects I have worked with before. You can see my experiences as
                cv/resume.
            </>
        ),
    },
];

function Feature({imageUrl, title, description}) {
    const imgUrl = useBaseUrl(imageUrl);
    return (
        <div className={clsx('col col--4', styles.feature)}>
            {imgUrl && (
                <div className="text--center">
                    <img className={styles.featureImage} src={imgUrl} alt={title}/>
                </div>
            )}
            <h3>{title}</h3>
            <p>{description}</p>
        </div>
    );
}

export default function Home() {
    const context = useDocusaurusContext();
    const {siteConfig = {}} = context;
    return (
        <Layout
            title={`Hello from ${siteConfig.title}`}
            description="Description will go into a meta tag in <head />">
            <header className={clsx('hero hero--primary', styles.heroBanner)}>
                <div className="container">
                    <p className="hero__subtitle">{siteConfig.tagline}</p>
                </div>
            </header>
            <main>
                {features && features.length > 0 && (
                    <section className={styles.features}>
                        <div className="container">
                            <div className="row">
                                {features.map((props, idx) => (
                                    <Feature key={idx} {...props} />
                                ))}
                            </div>
                        </div>
                    </section>
                )}
            </main>
        </Layout>
    );
}
