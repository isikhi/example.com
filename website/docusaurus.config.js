module.exports = {
  title: 'Huseyin ISIK',
  tagline: 'Highly determined backend-intensive software developer specializing in JavaScript with modern Tech/DevOps approaches.',
  url: 'https://isikhuseyin.com/',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'isikhi',
  projectName: 'isikhuseyin.com',
  themeConfig: {
    navbar: {
      title: 'Huseyin ISIK',
      logo: {
        alt: 'My Site Logo',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: 'blog',
          label: 'Blog',
          position: 'left'
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Blogs',
              to: 'blog/',
            },
          ],
        },
        {
          title: 'Social',
          items: [
            {
              label: 'Gitlab',
              href: 'https://gitlab.com/isikhi',
            },
            {
              label: 'Github',
              href: 'https://gitlab.com/isikhi',
            },
            {
              label: 'Stack Overflow',
              href: 'https://stackoverflow.com',
            },
            {
              label: 'Medium',
              href: 'https://medium.com/@huseyin.isik000',
            },
          ],
        },
        {
          title: 'Contact',
          items: [
            {
              label: 'Linkedin',
              href: 'https://www.linkedin.com/in/isikhi/',
            },
            {
              label: 'Mail',
              href: 'https://www.linkedin.com/in/isikhi/',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()}`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
